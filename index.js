const num = 2;
const getCube = num ** 3;
console.log(`the cube of ${num} is ${getCube}`);

const address = [258, "Washington Ave NW", "California", 90011]
const [streetNumber, streetName, state, zipCode] = address;
console.log(`i live at ${streetNumber} ${streetName} ${state} ${zipCode}`);

const animal = {
	name: "Lolong",
	animalType: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in "
}

const {name, animalType, weight, measurement} = animal;
console.log(`${name} was a ${animalType}. He weight at ${weight} with a measurement of ${measurement}`);

const numArray = [1, 2, 3, 4, 5];

numArray.forEach((nume) => console.log(`${nume}`));

class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const newDog = new Dog("frankie", 5, "miniature dachshund");
console.log(newDog);
